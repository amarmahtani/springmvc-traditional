<%--
  Created by IntelliJ IDEA.
  User: amahtani
  Date: 6/1/18
  Time: 22:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Spring MVC Tutorial</title>
</head>
<body>
<h1>Inside Hello JSP with Amar</h1>

<!-- This greetings is available in the model, that we populated in the Controller.-->
<h2>${greetings}</h2>
</body>
</html>
