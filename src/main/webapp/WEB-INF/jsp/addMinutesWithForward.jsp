<%--
  Created by IntelliJ IDEA.
  User: amahtani
  Date: 6/2/18
  Time: 23:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>

<%-- Lets use the Spring provided tag library that will help us easily pass data to the Controller --%>
<%@ taglib prefix="springform" uri="http://www.springframework.org/tags/form" %>
<%-- So basically this tag library is for forms, since we will be posting data to the Controller. --%>
<%-- Just as a note to self - the URI is not an external link, its an index to the springmvc JARs that came as part of Maven dependencies, so it just looks in there. --%>

<html>
<head>
    <title>Spring MVC Tutorial</title>

    <script language="JavaScript">
        function checkMinutes(elem) {
            if(elem.value > 60) {
                alert("Minutes cannot be above 60, please enter valid value.");
                elem.value = "";
            }
        }
    </script>

</head>
<body>
<h1>Add Minutes Exercised</h1>

<springform:form commandName="exerciseForward">

    <table>
        <tr>
            <td>Minutes: </td>
            <td><springform:input path="minutes" maxlength="2" type="number" onblur="checkMinutes(this)"/></td>

        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Submit Me"/>
            </td>
        </tr>
    </table>

    <br>
    <h3>Minutes obtained from Controller's model = ${minutesEntered}</h3>
    <br>

    <%-- Just how we displayed the Model from the original Servlet (controller method), we can also display
            from the forwarded Controller method.
    --%>
    <h3>Minutes obtained from Controller's Forwarded model = ${forwardedMinutesEntered}</h3>

</springform:form>

</body>
</html>
