<%--
  Created by IntelliJ IDEA.
  User: amahtani
  Date: 6/2/18
  Time: 23:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>

<%-- Lets use the Spring provided tag library that will help us easily pass data to the Controller --%>
<%@ taglib prefix="springform" uri="http://www.springframework.org/tags/form" %>
<%-- So basically this tag library is for forms, since we will be posting data to the Controller. --%>
<%-- Just as a note to self - the URI is not an external link, its an index to the springmvc JARs that came as part of Maven dependencies, so it just looks in there. --%>

<html>
<head>
    <title>Spring MVC Tutorial</title>

    <script language="JavaScript">
        function checkMinutes(elem) {
            if(elem.value > 60) {
                alert("Minutes cannot be above 60, please enter valid value.");
                elem.value = "";
            }
        }
    </script>

</head>
<body>
<h1>Add Minutes Exercised</h1>

<%-- So now we will use a Spring MVC form instead of HTML form, this way we can easily
bind the form data as an Object in the Controller. WOW!!!
--%>
<springform:form commandName="exercise">
                <%-- commandName ties in with @ModelAttribute in the Controller. --%>

    <table>
        <tr>
            <td>Minutes: </td>
            <td><springform:input path="minutes" maxlength="2" type="number" onblur="checkMinutes(this)"/></td>
                <%-- path ties in with the instance variable of the ModelAttribute POJO in the Controller.
                     maxlength is the basic html attribute.
                     type is also from HTML 5 world, basically marking this only as number field.
                     Placing Javascript just for fun, but Spring provides its way of validation. we will see that later.
                --%>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Submit Me"/>
            </td>
        </tr>
    </table>

    <%-- Simply displaying the value of the Minutes, that we get from the Model.
        - The first time we come to this page, it will be 0, since it is already bound an empty instance of Exercise (due to commandName).
        - The second time we come to this page are the form submit, the model will contain the value we entered in the
            springform:input field.
    --%>
    <br>
    <h3>Minutes obtained from Controller's model = ${minutesEntered}</h3>

</springform:form>

</body>
</html>
