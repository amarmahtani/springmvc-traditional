package org.java.amar.controller.lesson3Forwarder;

import org.java.amar.model.Exercise;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * In regular Servlets API, we map a URL pattern to a Servlet.
 * But in Spring MVC, we always want to Deal with DispatcherServlet and as per the exact URL, the specific Controller is chosen.
 *  URL Patter -> Dispatcher Servlet -> Exact Url -> Controller.
 *
 * Even our Spring MVC app can have other Servlets that work outside the Spring MVC framework, and if needed we can forward our request to those Servlets too.
 * Infact by default Spring MVC forward actually forwards to outside the Spring MVC framework, hence if we want to forward to Spring MVC, just make sure our forward uses the correct URL pattern.
 *
 *
 * Lets say we make the request directly to addMinutes, but we want addMinutes to forward to addMoreMinutes.
 * So all we need is just another request mapping method.
 */

@Controller
public class MinutesForwarderController {

    @RequestMapping(value = "/addMinutesWithForward")
    public String addMinutes(Model model,
                             @ModelAttribute("exerciseForward") Exercise exercise) {

        System.out.println("Exercise value from Form = "+exercise.getMinutes());

        model.addAttribute("minutesEntered", exercise.getMinutes());

        //SO now if we want to forward to addMoreMinutes, we just do this in the return string.
        return "forward:/app/addMoreMinutes";
            //SO based on the URL pattern it will forward back to the DispatcherServlet.
            //This way we can forward to an external servlet too.

        //For redirect, we just simply use redirect:xxxxx
        //Behavior will be different though, since a redirect, will complete out the request,
        // and in the response send a status code of 3xx with the new URL, which will make the browser send out a New request.
        // With the new request, our spring form will be reset and our model attribute wont be the same.

    }

    @RequestMapping(value = "/addMoreMinutes")
    public String addMoreMinutes(Model model,
                                 @ModelAttribute("exerciseForward") Exercise exercise) {
                            /*
                             * One Unexpected behavior is that Model is not shared across forwarded Controller methods.
                             * But I guess that is understandable, since we can also forward to outside the Spring mvc framework.
                             * So need to account for that.
                             * However if we continue to use @ModelAttribute, the framework will bind the Spring form to our Controller Method parameter.
                             * So this way we get access to the front end data.
                             */

        System.out.println("Inside Forwarded Controller method.");

        //For display purposes we will just square the minutes and add it to the model.
        int minutesEntered = exercise.getMinutes();

        int squaredValue = minutesEntered * minutesEntered;

        model.addAttribute("forwardedMinutesEntered", squaredValue);

        return "addMinutesWithForward"; //This will be the JSP page name.
    }

}
