package org.java.amar.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

//We mark this as a Controller, that we want the DispatcherServlet to forward requests to.
@Controller
public class HelloController {

    /**
     * What is this RequestMapping? It is how Spring will tie in our URL to this Method.
     * So when we go http://localhost:8080/app/greeting - App specifies the url-pattern for the DispatcherServlet, so engage it.
     *                                                    Greeting is this request mapping, so DispatcherServlet knows to execute this method.
     *
     * What is this Model? It is the Model that Spring provides to us, The DispatcherServlet sends it to us and our Service classes can fill it up.
     * For now we will treat it only as a Map. And Initially it is empty.
     * Once we return from this method, the Model is sent back to the Front Controller
     */
    @RequestMapping(value = "/greeting")
    public String sayHello(Model model) {

        //Lets Fill up the Model with stuff.
        model.addAttribute("greetings", "Hello World, From Amar - "+this); //I am just curious about the scope of this controller.
                                                                    //So by default all Controllers are Singletons and shared across the Application lifetime.
                                                                    //To make it Request or Session based, we will see it later. Basically use @Scope annotation.

        //What we are returning here is basically the name of the view - in this case it will equate to hello.jsp.
        return "hello";

        //A note on our JSPs. Good practice is to hide JSP files, and so we place them inside WEB-INF folder.
        //- This way they cannot directly go to that Page, or even bookmark to that page.
        //- They are always forced to go through the landing page, or through the framework. and this way we can guarantee user experience. (they wont see a broken page, etc).
        //But then to make them visible we need to do some magic :) Look at the servlet-config.
    }

    /**
     * What if we want the Method to return only a String, not the name of a view.
     * We can use the ResponseBody annotation.
     */
    @ResponseBody
    @RequestMapping(value="/greetingSimple")
    public String sayHelloSimple(Model model) {

        return "<h2>This is the string that will get wrapped in HTML and returned to the browser.</h2>";
    }

}
