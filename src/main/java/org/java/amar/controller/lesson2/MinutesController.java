package org.java.amar.controller.lesson2;

import org.java.amar.model.Exercise;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MinutesController {

    /**
     * How to pass Parameters from the View?
     * - We can just use HTML Input tags, with names, and those will be available in the Model.
     * - However Spring provides some tags that we can use to easily to interact with the Controller.
     *
     * - SO what Spring does is simple, It maps/binds a Spring Form to an existing POJO in the backend.
     * - In our Controller method, we mark our POJO with @ModelAttribute.
     * - In the View, under the corresponding Spring Form we provide an attribute `commandName` with the same name as @ModelAttribute.
     * - This now ties the JSP/Spring Form with a Backend Object/POJO.
     *
     * - Just as an Object contains instance variables, a Form contains input fields.
     * - So now to tie the Input field with an Object instance variable, we use the Spring's input tag, with the attribute `path` that has the same name as the instance variable's name.
     * @return
     */

    @RequestMapping(value = "/addMinutes")
    public String addMinutes(Model model,
                             @ModelAttribute ("exercise") Exercise exercise) {

        System.out.println("Exercise value from Form = "+exercise.getMinutes());

        //Lets place this value into our Model for the response.
        model.addAttribute("minutesEntered", exercise.getMinutes());

        return "addMinutes"; //This will be translated to the View - /WEB-INF/jsp/addMinutes.jsp
        //We will re-direct this response to the same page from which it came.
    }
}
