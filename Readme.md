## Spring MVC

* We have moved past to the REST based Spring web applications, but some older projects still use the Servlet based Spring web applications. This is what we will see here.
* Also here we are looking at the traditional way of doing it, soon we will see the Spring Boot way too.
* Spring MVC uses the Front Controller pattern, which is basically using one Servlet to handle all requests and then delegate/Dispatch the request accordingly. This is called the Dispatcher Servlet.

### Architecture of Spring MVC - How it is built over Servlet API

* Like any framework, it is built over the existing API - J2EE/Servlets API
* The framework Spring MVC basically provides conventions and standards on using J2EE/Servlets API (front controller pattern)


### Request/Response Life Cycle

* Request comes to the Front Controller (Dispatcher Servlet)
* The Front Controller then delegates the Request to a Controller (Simple POJO that follows Command Pattern that handles request and determines which view to use).
* The Controller then sends the Request to the backend for processing.
* The Backend returns a Model (a representation of the data that we want to send back.) to the Controller.
* The Controller does not care about rendering it (appending it with HTML tags, mustache template, etc.)
* The Controller sends the Model to the Front Controller and tells it which View to use.
* The Front Controller then sends this Model to a View Template for rendering. In this example we will use JSP
* The rendered View Template is sent back to the Front Controller and then Response is sent out.


### Other Elements of Spring MVC

* Servlet-config - Configuration file per DispatcherServlet. This is basically the Beans Configuration file.
* RequestMapping - The mapping between a Method and its associated URL and request type (POST, GET, etc.)
* ViewResolver - Used to locate JSP pages or whatever view we are using. (Mustache templates, thymeleaf templates, etc.)

### Getting Started

* Look at the web.xml and HelloController.java files.

### COMPONENTS (Layers) of Spring MVC

* So far we saw the @Controller class that gets the request from the DispatcherServlet and builds the Response.
** Business logic should NOT be in this level.
** Request & Response objects should end and begin at this level, Do not send them lower.
** Ideally this level should handle exceptions, and decide the View accordingly, success or failure.
** Again, Controller should Interpret exceptions from the Business logic layers.

* The next component that we have is the @Service class - which should ideally hold the Business logic.
** The scope of beans should begin at this level, not Controller. Request or Session.
** Controllers are by default Singletons that live across the Application (Level above Session)
** Spring MVC starts Transactions at the @Service class, this is helpful while accessing multiple database tables, etc.

* Finally we have the @Repository class component - This represents the DAO, ideally should be 1 to 1 mapping with a table.
** Main focus is ONLY interacting with database - persisting or reading - CRUD functions

### Controllers

* As seen in the examples and notes above, we see that Controllers are basically the Heart and Soul of Spring MVC.
* It receives the request, collects info for the Model (via @Service and @Repository), builds response and decides the View.
* So we will focus on Controllers now.
* Look at the MinutesController.java under lesson 2.
** Basic flow for this localhost:8080/app/addMinutes -> Controller:MinutesController::addMinutes() -> addMinutes.jsp
** Notes on how to pass parameters from the JSP page to Controller are also seen in this class.

### Servlet Forwarding

* If you recall in the basic Servlet API, they have the concept of Forwarding vs Redirect
* Forwarding is where the Servlet sends the req/resp to the next servlet, but the user is unaware - URL does not change on browser.
* Re-direct is where the Servlet sends back 3xx status code in response (Basically it is ending the request), and the browser will redirect to the new URL.
* So in Spring MVC, we do not really work with Servlets, except for the implicit Dispatcher Servlet.
* So how do we perform forwarding in Spring MVC? We do it within the Controller. Look at Controller in Lesson 4.

### Interceptors

* Intercepts the request before reaching the Controller and can also intercept the response before reaching the browser.
* Basically ability to per-handle and post-handle the web request.

### Views

* So far we saw the Internal View Resolver, and that it resolves to files under /WEB-INF/jsp/xxxx.jsp
* So are we stuck with only JSPs? Not quite, we can use difference view resolvers that also point to different front end technologies, like pure HTML, JSF, Tiles, FreeMarker, etc.
* As a side note, Spring itself provides many types of View Resolvers, and also allows us to create our own.